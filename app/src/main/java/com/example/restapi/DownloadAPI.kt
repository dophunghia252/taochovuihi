package com.example.restapi

import com.google.gson.annotations.SerializedName

class DownloadAPI  {
    @SerializedName("data")
    var data : ArrayList<DataDownload>?= null


}
class DataDownload {
    @SerializedName("id")
    var id : Int = 0
    @SerializedName("name")
    var name : String = ""
    @SerializedName("time_create")
    var timeCreate : String = ""
    @SerializedName("version")
    var vesSion : String = ""
    @SerializedName("image")
    var image : ArrayList<String>?= null
    @SerializedName("category")
    var cateGory : ArrayList<Category>?= ArrayList()

}
class Category {
    @SerializedName("id")
    var id : Int = 0
    @SerializedName("name")
    var name : String = ""
}

