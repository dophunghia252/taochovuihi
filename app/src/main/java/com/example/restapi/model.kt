package com.example.restapi

import com.google.gson.annotations.SerializedName

class model (

    val id : Int ,
    @SerializedName("title")
    val mtitle : String,
    @SerializedName("body")
    val mbody : String
        )