package com.example.restapi

import retrofit2.Call
import retrofit2.http.GET

interface jsonrestapi {
    //https://jsonplaceholder.typicode.com/posts
    @GET("posts")
    fun getInfo() : Call<List<model>>
    @GET("/api/v1/list-sugestion/2")
    fun getDownload() : Call<DownloadAPI>
}