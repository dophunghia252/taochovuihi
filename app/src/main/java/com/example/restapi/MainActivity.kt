package com.example.restapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.widget.TextView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {
    var textView : TextView?= null
    var mdownloadapi : ArrayList<DataDownload>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.txtUser)

        getAPIdownload()

    }
    fun getAPIdownload() {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://104.154.22.26")
            .build()
        val jsonrestapi = retrofit.create(jsonrestapi::class.java)
        val mcall: Call<DownloadAPI> = jsonrestapi.getDownload()
        mcall.enqueue(object : Callback<DownloadAPI> {
            override fun onResponse(call: Call<DownloadAPI>, response: Response<DownloadAPI>) {
                mdownloadapi?.clear()
                mdownloadapi = response.body()?.data
                val stringBuilder = StringBuilder()
                mdownloadapi?.forEach {
                    stringBuilder.append(it.id)
                    stringBuilder.append("\n")
                    stringBuilder.append(it.name)
                    stringBuilder.append("\n")
                    stringBuilder.append(it.timeCreate)
                    stringBuilder.append("\n")
                    stringBuilder.append(it.vesSion)
                    stringBuilder.append("\n")
                    stringBuilder.append(it.image)
                    stringBuilder.append("\n")
                    stringBuilder.append(it.cateGory?.get(0)?.id.toString())
                }
                textView?.movementMethod = ScrollingMovementMethod()
                textView?.text = stringBuilder
            }
            override fun onFailure(call: Call<DownloadAPI>, t: Throwable) {
                Log.e("Error", t.message.toString())
            }
        }
        )

    }
}